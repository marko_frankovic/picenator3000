package Probni_kod;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LoginFrame extends JFrame implements ActionListener {

    JLabel userNameLable, passwordLabel;
    JTextField userNameTextField;
    JPasswordField passwordField;
    JButton loginButton;
    Container container;

    public LoginFrame() {
        userNameLable = new JLabel("Korisničko ime");
        userNameTextField = new JTextField();
        passwordLabel = new JLabel("Lozinka");
        passwordField = new JPasswordField();
        loginButton = new JButton("Unesi");
        container = getContentPane();
        container.setLayout(null);
        setBounds();
        addComponents();
        addActionListener();

    }

    public void setBounds() {
        userNameLable.setBounds(10, 10, 100, 30);
        userNameTextField.setBounds(100, 10, 200, 30);
        passwordLabel.setBounds(10, 50, 100, 30);
        passwordField.setBounds(100, 50, 200, 30);
        loginButton.setBounds(100, 100, 200, 30);
    }

    public void addComponents() {
        container.add(userNameLable);
        container.add(userNameTextField);
        container.add(passwordLabel);
        container.add(passwordField);
        container.add(loginButton);
    }

    public void addActionListener() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public static void main(String[] args) {
        LoginFrame frame = new LoginFrame();
        frame.setTitle("Pičenator 3000");
        frame.setVisible(true);
        frame.setBounds(250, 250, 500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }

}