package Probni_kod;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Citanje {

        private static void readUsingClass(String fileName)
                throws IOException
        {

            Path path = Paths.get(fileName);


            byte[] bytes = Files.readAllBytes(path);


            @SuppressWarnings("unused")

            // Creating a List class object of string type
            // as data in file to be read is words
            List<String> allLines = Files.readAllLines(
                    path, StandardCharsets.UTF_8);
            System.out.println(new String(bytes));
        }


        public static void main(String[] args)
                throws IOException
        {

            String fileName
                    = "C:\\Users\\Marko\\Desktop\\Pjesme\\databaza.txt";


            readUsingClass(fileName);
        }

}
