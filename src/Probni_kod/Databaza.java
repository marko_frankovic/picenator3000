package Probni_kod;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Databaza {


    public void AddRecord() throws IOException {
    String ime_pjesme,ocjena;

    BufferedWriter bw = new BufferedWriter( new FileWriter("C:\\Users\\Marko\\Desktop\\Pjesme\\databaza.txt",true) );
    Scanner strInput = new Scanner(System.in);



    		System.out.print("Upisite novu pjesmu: ");
    ime_pjesme = strInput.nextLine();

    		System.out.print("Upisite ocjenu pjesme: ");
    ocjena = strInput.nextLine();

    		bw.write(ime_pjesme+","+ocjena);
    		bw.flush();
    		bw.newLine();
    		bw.close();

}


    public void ViewAllRecord() throws IOException {
        BufferedReader br = new BufferedReader( new FileReader("C:\\Users\\Marko\\Desktop\\Pjesme\\databaza.txt") );

        String record;

        System.out.println(" --------------------------- ");
        System.out.println("|  Ime Pjesme 		Ocjena	|");
        System.out.println(" --------------------------- ");

        while( ( record = br.readLine() ) != null ) {

            StringTokenizer st = new StringTokenizer(record,",");

            System.out.println("|	"+st.nextToken()+" 		"+st.nextToken()+"	     |");

        }

        System.out.println("|                           |");
        System.out.println(" --------------------------- ");
        br.close();

    }


    public void DeleteRecordByID() throws IOException {
        Scanner strInput =  new Scanner(System.in);
        String ime_pjesme, record;


        File tempDB = new File("C:\\Users\\Marko\\Desktop\\Pjesme\\databaza_2.txt");
        File db = new File("C:\\Users\\Marko\\Desktop\\Pjesme\\databaza.txt");


        BufferedReader br = new BufferedReader( new FileReader( db ) );
        BufferedWriter bw = new BufferedWriter( new FileWriter( tempDB ) );


        System.out.println("\t\t Izbriši pjesmu iz databaze\n");

        System.out.println("Upisite ime pjesme: ");
        ime_pjesme =  strInput.nextLine();


        while( ( record = br.readLine() ) != null ) {


            if( record.contains(ime_pjesme) )
                continue;

            bw.write(record);
            bw.flush();
            bw.newLine();

        }

        br.close();
        bw.close();

        db.delete();

        tempDB.renameTo(db);

    }



    public void SearchRecordbyID() throws IOException {
        String ime_pjesme, record;
        Scanner strInput = new Scanner(System.in);

        BufferedReader br = new BufferedReader( new FileReader("C:\\Users\\Marko\\Desktop\\Pjesme\\databaza.txt") );

        System.out.println("\t\t Pretraži databazu\n");


        System.out.println("Upisite ime pjesme: ");
        ime_pjesme = strInput.nextLine();

        System.out.println(" --------------------------- ");
        System.out.println("|  Ime Pjesme 		Ocjena	|");
        System.out.println(" --------------------------- ");

        while( ( record = br.readLine() ) != null ) {

            StringTokenizer st = new StringTokenizer(record,",");
            if( record.contains(ime_pjesme) ) {
                System.out.println("|	"+st.nextToken()+" 		"+st.nextToken()+"	     |");
            }



        }

        System.out.println("|                           |");
        System.out.println(" --------------------------- ");

        br.close();

    }


    public void updateRecordbyID() throws IOException {
        String novo_ime_pjesme, novi_rating,record, ime_pjesme,record2;

        File db = new File("C:\\Users\\Marko\\Desktop\\Pjesme\\databaza.txt");
        File tempDB = new File("C:\\Users\\Marko\\Desktop\\Pjesme\\databaza_2.txt");

        BufferedReader br = new BufferedReader( new FileReader(db) );
        BufferedWriter bw = new BufferedWriter( new FileWriter(tempDB) );

        Scanner strInput = new Scanner(System.in);

        System.out.println("\t\t Uredi pjesmu \n\n");
        /****/
        System.out.println("Upisi ime pjesme: ");
        ime_pjesme = strInput.nextLine();
        System.out.println(" --------------------------- ");
        System.out.println("|  Ime Pjesme 		Ocjena	|");
        System.out.println(" --------------------------- ");;

        while( ( record = br.readLine() ) != null ) {

            StringTokenizer st = new StringTokenizer(record,",");
            if( record.contains(ime_pjesme) ) {
                System.out.println("|	"+st.nextToken()+" 		"+st.nextToken()+"	     |");
            }

        }
        System.out.println("|                           |");
        System.out.println(" --------------------------- ");

        br.close();
        /****/
        System.out.println("Upisite novo ime pjesme: ");
        novo_ime_pjesme = strInput.nextLine();
        System.out.println("Upisite novu ocjenu: ");
        novi_rating = strInput.nextLine();


        BufferedReader br2 = new BufferedReader( new FileReader(db) );

        while( (record2 = br2.readLine() ) != null ) {
            if(record2.contains(ime_pjesme)) {
                bw.write(novo_ime_pjesme+","+novi_rating);
            } else {

                bw.write(record2);
            }
            bw.flush();
            bw.newLine();
        }

        bw.close();
        br2.close();
        db.delete();
        boolean success = tempDB.renameTo(db);
        System.out.println(success);

    }

    public static void main(String[] args) throws IOException {
        Databaza baza = new Databaza();
        Scanner strInput = new Scanner(System.in);
        String choice,cont = "y";

        while( cont.equalsIgnoreCase("y") ) {
            System.out.println("\t\t Infomracije databaze\n\n");

            System.out.println("1 ===> Dodaj novu pjesmu ");
            System.out.println("2 ===> Pregledaj sve pjesme ");
            System.out.println("3 ===> Obriši pjesmu ");
            System.out.println("4 ===> Pretraži pjesmu ");
            System.out.println("5 ===> Uredi pdabranu pjesmu ");

            System.out.print("\n\n");
            System.out.println("Upisite svoj izbor: ");
            choice = strInput.nextLine();

            if( choice.equals("1") ) {
                baza.AddRecord();
            } else if( choice.equals("2") ) {
                baza.ViewAllRecord();
            } else if( choice.equals("3") ) {
                baza.DeleteRecordByID();
            }	else if( choice.equals("4") ) {
                baza.SearchRecordbyID();
            }	else if( choice.equals("5") ) {
                baza.updateRecordbyID();
            }

            System.out.println("Želite li nastaviti? Y/N");
            cont = strInput.nextLine();

        }

    }
    }




