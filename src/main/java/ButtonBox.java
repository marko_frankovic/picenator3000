import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.*;
import javax.swing.Timer;

class Picenator {

    //Admin sučelje
    public static void AdminOkvir() throws IOException {
        Button adminButton;
        JFrame container;
        String ime;
        String lozinka;

        adminButton = new Button("Promjeni rating");
        adminButton.setBounds(100, 100, 200, 30);

        adminButton.addActionListener(e -> {
            try {
                Runtime.getRuntime().exec("notepad src/main/resources/databaza.txt");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        container = new JFrame();
        container.add(adminButton);
        container.setLayout(null);
        container.setTitle("Pičenator 3000 by Marko Franković");
        container.setLocationByPlatform(true);
        container.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        container.setResizable(true);
        container.pack();
        container.setVisible(true);
        container.setMinimumSize(new Dimension(400, 200));

    }


    //Dodavanje pjesama
    public static final String[] media = {

            //stock_cola
            "src/main/resources/stock_cola/CECA - Autogram (Official) 2016.wav",
            "src/main/resources/stock_cola/Ceca - Da raskinem sa njom - (Official Video 2013) HD.wav",
            "src/main/resources/stock_cola/Darko Lazic - Godinu dana 300 kafana.wav",
            "src/main/resources/stock_cola/Djani - Ja stalno pijem.wav",
            "src/main/resources/stock_cola/Djani - Litar na litar - (Audio 2001).wav",
            "src/main/resources/stock_cola/Dzej i Mina - Slavija - (Tv Pink).wav",
            "src/main/resources/stock_cola/Haris Dzinovic - Pariske kapije.wav",
            "src/main/resources/stock_cola/Lepa Brena - Luda za tobom - (Audio 2004).wav",
            "src/main/resources/stock_cola/Mile Kitic - Plava ciganka - (Audio 2001).wav",
            "src/main/resources/stock_cola/Rada Manojlovic - Mesaj, mala (Duet Sasa Matic) - (Audio 2011).wav",

            //jack_sa_ledom
            "src/main/resources/jack_sa_ledom/Buba Corelli - Gluh i Nijem.wav",
            "src/main/resources/jack_sa_ledom/Buba Corelli - Irina Shayk.wav",
            "src/main/resources/jack_sa_ledom/Buba Corelli ft. Jala Brat & Coby - Divljam.wav",
            "src/main/resources/jack_sa_ledom/CONNECT FEAT. COBY - DZEK I DZONI (OFFICIAL VIDEO).wav",
            "src/main/resources/jack_sa_ledom/DEVITO x TEODORA - VUDU.wav",
            "src/main/resources/jack_sa_ledom/Jala Brat & Buba Corelli - Klinka.wav",
            "src/main/resources/jack_sa_ledom/Jala Brat & Buba Corelli ft. Rasta - Benga po snijegu.wav",
            "src/main/resources/jack_sa_ledom/MC STOJAN - LA MIAMI (OFFICIAL VIDEO) 4K.wav",
            "src/main/resources/jack_sa_ledom/MC STOJAN X HURRICANE - TUTURUTU (OFFICIAL VIDEO).wav",
            "src/main/resources/jack_sa_ledom/TEODORA X MC STOJAN - VOLI ME, VOLI ME (OFFICIAL VIDEO).wav",

            //travarica
            "src/main/resources/rakija/Alfa Time Band - Biska (official video).wav",
            "src/main/resources/rakija/Bora Drljaca - Otisla je Danijela - (Audio 2007).wav",
            "src/main/resources/rakija/Bora Drljaca - Prodaj majko Golfa dizelasa - GS 2012_2013 - 23.11.2012. EM 8.wav",
            "src/main/resources/rakija/I Tebe Sam Sit Kafano.wav",
            "src/main/resources/rakija/Lepa Brena - Kolovodja - (Audio 2001).wav",
            "src/main/resources/rakija/Lepa Brena - Mile voli disko - (Audio 1995) HD.wav",
            "src/main/resources/rakija/Miroslav Ilic - Polomicu case od kristala - (Audio 1979) HD.wav",
            "src/main/resources/rakija/Mitar Miric - Ne moze nam niko nista - (Audio 1989) HD.wav",
            "src/main/resources/rakija/Pijem na ex.wav",
            "src/main/resources/rakija/Viki Miljkovic - Nikom nije lepse nego nama - (Audio 2011).wav",

            //vino
            "src/main/resources/vino/Goran Dimitrijadis Dima - Nije Mene Dušo Ubilo (OFFICIAL VIDEO) AUDIO REMASTER 2020.wav",
            "src/main/resources/vino/Haris Dzinovic - Mustuluk (2009).wav",
            "src/main/resources/vino/Lepa Brena - Hajde da se volimo - (Official Video 1987).wav",
            "src/main/resources/vino/miroslav ilić - nije život jedna žena.wav",
            "src/main/resources/vino/Pola vino pola voda.wav",
            "src/main/resources/vino/Sejo Pitić - Plava žena - topla zima  (Audio 1980).wav",
            "src/main/resources/vino/Stari Prijatelji - Ja još uvijek kao momak živim.wav",
            "src/main/resources/vino/Tozovac - Ti Si Me Čekala.wav",
            "src/main/resources/vino/Tozovac -  prazna casa na mom stolu.wav",
            "src/main/resources/vino/Vlado Kalember - Vino na usnama (Video 1988).wav",

            //jaeger_cola
            "src/main/resources/jaeger_cola/Bane Bojanic - Samo pijan mogu da prebolim - (Audio 2001).wav",
            "src/main/resources/jaeger_cola/Darko Lazic - Idi drugome - (Audio 2009).wav",
            "src/main/resources/jaeger_cola/Funky G - Kafana na Balkanu - (Audio 2008) HD.wav",
            "src/main/resources/jaeger_cola/Indira - Lopov - (Audio 2002).wav",
            "src/main/resources/jaeger_cola/Jana - Barabar - (Audio 2001).wav",
            "src/main/resources/jaeger_cola/Mile Kitic - Milioni, kamioni - (Audio 2004).wav",
            "src/main/resources/jaeger_cola/Milica Todorovic - Uporedi me - (Audio 2012).wav",
            "src/main/resources/jaeger_cola/Sanja Djordjevic - Poruci pesmu sa imenom mojim.wav",
            "src/main/resources/jaeger_cola/Sinan Sakic - Lepa do bola.wav",
            "src/main/resources/jaeger_cola/STOJA - Bela ciganka - (Audio 2013).wav",

            //bambus
            "src/main/resources/bambus/A U Medjuvremenu.wav",
            "src/main/resources/bambus/Halid Beslic - Miljacka - (Audio 2008).wav",
            "src/main/resources/bambus/Halid Beslic - Prvi poljubac - (Official Video).wav",
            "src/main/resources/bambus/Halid bešlic  IZNAD TESNJA.wav",
            "src/main/resources/bambus/Jana - Sta ce ti pevacica - (Audio 2002).wav",
            "src/main/resources/bambus/Lepa Brena - Cik pogodi - (Audio 2004).wav",
            "src/main/resources/bambus/Lepa Brena - Duge noge - (Audio 2004).wav",
            "src/main/resources/bambus/Miroslav Ilic - Pozdravi je pozdravi - (Audio 1983) HD.wav",
            "src/main/resources/bambus/Miroslav ilić - luckasta si ti.wav",
            "src/main/resources/bambus/Vesna Zmijanac - Kazni me, kazni - (Official Video 1989).wav",

            //gin_tonic
            "src/main/resources/gin_tonic/Dara Bubamara - Zidovi - (Audio 2007).wav",
            "src/main/resources/gin_tonic/Dragana Mirkovic-Poslednje vece.wav",
            "src/main/resources/gin_tonic/Emir Đulović &Andreana Čekić cipele.wav",
            "src/main/resources/gin_tonic/Maya - Dzin i limunada - (Audio 2007) (online-audio-converter.com).wav",
            "src/main/resources/gin_tonic/Natasa Bekvalac - Nikotin - (Audio 2004) HD.wav",
            "src/main/resources/gin_tonic/Natasa Bekvalac - Ponovo - (Audio 2004) HD.wav",
            "src/main/resources/gin_tonic/Predrag Zivkovic Tozovac - Uzmi sve sto ti zivot pruza - (Audio 2013) HD.wav",
            "src/main/resources/gin_tonic/Seka - Aspirin - (Audio 2007).wav",
            "src/main/resources/gin_tonic/Sinan Sakic - Minut, dva.wav",
            "src/main/resources/gin_tonic/Tanja Savic - Tako mlada - (Audio 2005).wav",

            //vodka_juice
            "src/main/resources/vodka_juice/Djani - Sve mi tvoje nedostaje - (Audio 2005).wav",
            "src/main/resources/vodka_juice/Mile Kitic - Sanker - (Audio 2008).wav",
            "src/main/resources/vodka_juice/Milica Todorovic - Tri case - (Official Video 2013) HD (online-audio-converter.com).wav",
            "src/main/resources/vodka_juice/Mitar Miric - Doberman - (Audio 2011) HD.wav",
            "src/main/resources/vodka_juice/Sanja Djordjevic - Crveni lak - (Audio 2005).wav",
            "src/main/resources/vodka_juice/Seka Aleksic - Crno i zlatno - (Audio 2003).wav",
            "src/main/resources/vodka_juice/SEKA ALEKSIC - CRVENI RUZ (OFFICIAL VIDEO).wav",
            "src/main/resources/vodka_juice/Stoja - Potopicu ovaj splav - (Audio 2008).wav",
            "src/main/resources/vodka_juice/Tamara Milutinovic - Hajde da zazmurimo (Official Video 2017).wav",
            "src/main/resources/vodka_juice/ĐOGANI ft. Mile Kitić - Nema više cile-mile - Official video HD.wav",

            //voda
            "src/main/resources/voda/Alen Vitasović, Marco Grabber & Ivan Arnold - Oštarije Su Mi Zaprli (Official Music Video).wav",
            "src/main/resources/voda/Budi Moja Voda.wav",
            "src/main/resources/voda/IVAN ZAK - VODU NE PIJEM.wav",
            "src/main/resources/voda/KUMOVI - DA SAM ČESTO TRIJEZAN (Official Video).wav",
            "src/main/resources/voda/Lucija Lu Jakelić - Oblik Vode.wav",
            "src/main/resources/voda/Marko Škugor - Kap vode (Official Video).wav",
            "src/main/resources/voda/Neno Belan & Fiumens - RIJEKA SNOVA.wav",
            "src/main/resources/voda/Sinan Sakić - Trezan - (Audio 2002).wav",
            "src/main/resources/voda/Vanna - Kao rijeka.wav",
            "src/main/resources/voda/Željko Bebek - Dabogda te voda odnijela.wav"




    };


    //Timer
    static Timer timer;
    //Broj klipa
    public static int currentClipIndex;

    //Load fileova
    public static Clip loadClip( String filename )
    {
        Clip in = null;

        try
        {
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(Objects.requireNonNull(new File(filename)));
            in = AudioSystem.getClip();
            in.open( audioIn );
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }

        return in;
    }

    //Varijabla korištena za pozivanje databaze
    static String record;

    //Pičenator korisnik sučelje
    public static void Sucelje() throws IOException {


        ArrayList<Clip> clips = new ArrayList<>();

        for (int a =0;a<90;a++) {
            clips.add(loadClip(media[a]));
        }


        currentClipIndex = 0;


        ArrayList <Integer> Rating = new ArrayList<>();
        ArrayList <String> Imena = new ArrayList<>();


        //Buffer za pozivanje databaze
        BufferedReader br = new BufferedReader( new FileReader("src/main/resources/databaza.txt") );
        File db = new File("src/main/resources/databaza.txt");

        File tempDB = new File("src/main/resources/databaza_2.txt");

        BufferedWriter bw = new BufferedWriter( new FileWriter(tempDB) );

        while( ( record = br.readLine() ) != null ) {
            StringTokenizer st = new StringTokenizer(record,",");
            Imena.add(st.nextToken());
            Rating.add(Integer.valueOf(st.nextToken()));
        }


        //Sortiranje pjesma po ratingu
        for (int a =0;a<9;a++) {
            for (int i = 10*a; i < 10*(a+1); i++) {
                for (int j = 10*a; j < 10*(a+1); j++) {
                    if (Rating.get(i) > Rating.get(j)) {
                        int o = Rating.get(j);
                        String s = Imena.get(j);
                        Clip u = clips.get(j);


                        clips.set(j, clips.get(i));
                        clips.set(i, u);

                        Rating.set(j, Rating.get(i));
                        Rating.set(i, o);

                        Imena.set(j, Imena.get(i));
                        Imena.set(i, s);
                    }
                }
            }
        }

        //Definiranje ikona
        ImageIcon imageIcon1 = new ImageIcon("src/main/resources/Gumbovi/stock.jpg");
        Image image1 = imageIcon1.getImage(); // transform it
        Image newimg1 = image1.getScaledInstance(100, 100,  Image.SCALE_SMOOTH);
        imageIcon1 = new ImageIcon(newimg1);

        ImageIcon imageIcon2 = new ImageIcon("src/main/resources/Gumbovi/jack_s_ledom.jpg");
        Image image2 = imageIcon2.getImage(); // transform it
        Image newimg2 = image2.getScaledInstance(100, 100,  Image.SCALE_SMOOTH);
        imageIcon2 = new ImageIcon(newimg2);

        ImageIcon imageIcon3 = new ImageIcon("src/main/resources/Gumbovi/travarica.jpg");
        Image image3 = imageIcon3.getImage(); // transform it
        Image newimg3 = image3.getScaledInstance(100, 100,  Image.SCALE_SMOOTH);
        imageIcon3 = new ImageIcon(newimg3);

        ImageIcon imageIcon4 = new ImageIcon("src/main/resources/Gumbovi/merlot.jpg");
        Image image4 = imageIcon4.getImage(); // transform it
        Image newimg4 = image4.getScaledInstance(100, 100,  Image.SCALE_SMOOTH);
        imageIcon4 = new ImageIcon(newimg4);

        ImageIcon imageIcon5 = new ImageIcon("src/main/resources/Gumbovi/đeger-cola.jpg");
        Image image5 = imageIcon5.getImage(); // transform it
        Image newimg5 = image5.getScaledInstance(100, 100,  Image.SCALE_SMOOTH);
        imageIcon5 = new ImageIcon(newimg5);

        ImageIcon imageIcon6 = new ImageIcon("src/main/resources/Gumbovi/bambus.jpg");
        Image image6 = imageIcon6.getImage(); // transform it
        Image newimg6 = image6.getScaledInstance(100, 100,  Image.SCALE_SMOOTH);
        imageIcon6 = new ImageIcon(newimg6);

        ImageIcon imageIcon7 = new ImageIcon("src/main/resources/Gumbovi/gin-tonic.jpg");
        Image image7 = imageIcon7.getImage(); // transform it
        Image newimg7 = image7.getScaledInstance(100, 100,  Image.SCALE_SMOOTH);
        imageIcon7 = new ImageIcon(newimg7);

        ImageIcon imageIcon8 = new ImageIcon("src/main/resources/Gumbovi/vodka-juice.jpg");
        Image image8 = imageIcon8.getImage(); // transform it
        Image newimg8 = image8.getScaledInstance(100, 100,  Image.SCALE_SMOOTH);
        imageIcon8 = new ImageIcon(newimg8);

        ImageIcon imageIcon9 = new ImageIcon("src/main/resources/Gumbovi/voda.jpg");
        Image image9 = imageIcon9.getImage(); // transform it
        Image newimg9 = image9.getScaledInstance(100, 100,  Image.SCALE_SMOOTH);
        imageIcon9 = new ImageIcon(newimg9);

        ImageIcon imageIcon10 = new ImageIcon("src/main/resources/Gumbovi/play.png");
        Image image10 = imageIcon10.getImage(); // transform it
        Image newimg10 = image10.getScaledInstance(20, 20,  Image.SCALE_SMOOTH);
        imageIcon10 = new ImageIcon(newimg10);

        ImageIcon imageIcon11 = new ImageIcon("src/main/resources/Gumbovi/pause.png");
        Image image11 = imageIcon11.getImage(); // transform it
        Image newimg11 = image11.getScaledInstance(20, 20,  Image.SCALE_SMOOTH);
        imageIcon11 = new ImageIcon(newimg11);

        ImageIcon imageIcon12 = new ImageIcon("src/main/resources/Gumbovi/next.png");
        Image image12 = imageIcon12.getImage(); // transform it
        Image newimg12 = image12.getScaledInstance(20, 20,  Image.SCALE_SMOOTH);
        imageIcon12 = new ImageIcon(newimg12);

        ImageIcon imageIcon13 = new ImageIcon("src/main/resources/Gumbovi/prev.png");
        Image image13 = imageIcon13.getImage(); // transform it
        Image newimg13 = image13.getScaledInstance(20, 20,  Image.SCALE_SMOOTH);
        imageIcon13 = new ImageIcon(newimg13);

        ImageIcon imageIcon14 = new ImageIcon("src/main/resources/Gumbovi/restart.png");
        Image image14 = imageIcon14.getImage(); // transform it
        Image newimg14 = image14.getScaledInstance(20, 20,  Image.SCALE_SMOOTH);
        imageIcon14 = new ImageIcon(newimg14);

        //Definiranje panela gumbova
        GridBagConstraints c = new GridBagConstraints();

        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        JPanel glazba = new JPanel();

        JTextField field = new JTextField();
        JTextField rating = new JTextField();

        BoxLayout layout1 = new BoxLayout(panel1, BoxLayout.Y_AXIS);
        BoxLayout layout2 = new BoxLayout(panel2, BoxLayout.Y_AXIS);
        BoxLayout layout3 = new BoxLayout(panel3, BoxLayout.Y_AXIS);

        GridBagLayout playertekst = new GridBagLayout();


        panel1.setLayout(layout1);
        panel2.setLayout(layout2);
        panel3.setLayout(layout3);
        glazba.setLayout(playertekst);


        //Definiranje gumbova
        JButton button1 = new JButton(imageIcon1);
        JButton button2 = new JButton(imageIcon2);
        JButton button3 = new JButton(imageIcon3);
        JButton button4 = new JButton(imageIcon4);
        JButton button5 = new JButton(imageIcon5);
        JButton button6 = new JButton(imageIcon6);
        JButton button7 = new JButton(imageIcon7);
        JButton button8 = new JButton(imageIcon8);
        JButton button9 = new JButton(imageIcon9);
        JButton button10 = new JButton(imageIcon10);
        JButton button11 = new JButton(imageIcon11);
        JButton button12 = new JButton(imageIcon12);
        JButton button18 = new JButton(imageIcon13);
        JButton button19 = new JButton(imageIcon14);
        JButton button13 = new JButton("1");
        JButton button14 = new JButton("2");
        JButton button15 = new JButton("3");
        JButton button16 = new JButton("4");
        JButton button17 = new JButton("5");

        button1.setAlignmentX(Component.LEFT_ALIGNMENT);
        button2.setAlignmentX(Component.LEFT_ALIGNMENT);
        button3.setAlignmentX(Component.LEFT_ALIGNMENT);

        panel1.add(button1);
        panel1.add(button2);
        panel1.add(button3);

        button4.setAlignmentX(Component.CENTER_ALIGNMENT);
        button5.setAlignmentX(Component.CENTER_ALIGNMENT);
        button6.setAlignmentX(Component.CENTER_ALIGNMENT);

        panel2.add(button4);
        panel2.add(button5);
        panel2.add(button6);

        button7.setAlignmentX(Component.RIGHT_ALIGNMENT);
        button8.setAlignmentX(Component.RIGHT_ALIGNMENT);
        button9.setAlignmentX(Component.RIGHT_ALIGNMENT);

        panel3.add(button7);
        panel3.add(button8);
        panel3.add(button9);

        //Definiranje pozicija gumbova u polju
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        glazba.add(button10, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 0;
        glazba.add(button11, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 3;
        c.gridy = 0;
        glazba.add(button12, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 2;
        c.gridy = 0;
        glazba.add(button18, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 4;
        c.gridy = 0;
        glazba.add(button19, c);


        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 40;
        c.ipadx = 350;
        c.weightx = 0.0;
        c.gridwidth = 6;
        c.gridx = 0;
        c.gridy = 1;
        glazba.add(field, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 40;
        c.weightx = 0.0;
        c.gridwidth = 6;
        c.gridx = 0;
        c.gridy = 2;
        glazba.add(rating, c);


        //Definiranje rada timera
        timer = new Timer(0,(e -> {
            clips.get(currentClipIndex).stop();
            if ((currentClipIndex+1)%10 == 0){
                currentClipIndex-=10;
            }
            currentClipIndex+=1;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            timer.start();
        }));

        timer.setRepeats(false);

        //Dodavanje fukcija na gumbove pića
        button1.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            currentClipIndex = 0;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();
        });

        button2.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            currentClipIndex = 10;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        button3.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            currentClipIndex = 20;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        button4.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            currentClipIndex = 30;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        button5.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            currentClipIndex = 40;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        button6.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            currentClipIndex = 50;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        button7.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            currentClipIndex = 60;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        button8.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            currentClipIndex = 70;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        button9.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            currentClipIndex = 80;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        //Start gumb
        button10.addActionListener(e -> {
            clips.get(currentClipIndex).start();
            if (!timer.isRunning()){
                timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()-clips.get(currentClipIndex).getMicrosecondPosition())/1000);
                timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()-clips.get(currentClipIndex).getMicrosecondPosition())/1000);
                timer.start();
            }
        });

        //Pause gumb
        button11.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            timer.stop();
        });

        //Skip gumb
        button12.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            if ((currentClipIndex+1)%10 == 0){
                currentClipIndex-=10;
            }
            currentClipIndex+=1;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        //Previous gumb
        button18.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            if ((currentClipIndex-1)%10 == 9 || (currentClipIndex-1)<0){
                currentClipIndex+=10;
            }
            currentClipIndex-=1;
            clips.get(currentClipIndex).setFramePosition(0);
            timer.setDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            timer.setInitialDelay((int) (clips.get(currentClipIndex).getMicrosecondLength()/1000));
            field.setText("Slušate: " + Imena.get(currentClipIndex));
            rating.setText(String.valueOf(Rating.get(currentClipIndex)));
            timer.start();

        });

        //Restart gumb
        button19.addActionListener(e -> {
            clips.get(currentClipIndex).stop();
            clips.get(currentClipIndex).setFramePosition(0);
            timer.restart();
        });


        //Definiranje okvira sučelja
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.add(panel1);
        frame.add(panel2);
        frame.add(panel3);
        frame.add(glazba);
        frame.setTitle("Pičenator 3000 by Marko Franković");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationByPlatform(true);
        frame.setSize(1000, 500);
        frame.setResizable(true);
        frame.pack();
        frame.setVisible(true);

    }

    //Login sučelje
    static class Login{
        JLabel userNameLable, passwordLabel;
        JTextField userNameTextField;
        JPasswordField passwordField;
        JButton loginButton;
        JFrame container;
        String ime;
        String lozinka;

        //Definiranje login sučelja
        public void LoginOkvir() {
            userNameLable = new JLabel("Korisničko ime");
            userNameTextField = new JTextField();

            passwordLabel = new JLabel("Lozinka");
            passwordField = new JPasswordField();

            loginButton = new JButton("Unesi");
            userNameLable.setBounds(10, 10, 100, 30);
            userNameTextField.setBounds(100, 10, 200, 30);
            passwordLabel.setBounds(10, 50, 100, 30);
            passwordField.setBounds(100, 50, 200, 30);
            loginButton.setBounds(100, 100, 200, 30);

            //Uvjeti ovisini o upisanom
            loginButton.addActionListener(e -> {
                ime = userNameTextField.getText();
                lozinka = passwordField.getText();
                if (ime.equals("korisnik") && lozinka.equals("korisnik")){
                    try {
                        Sucelje();
                        container.setVisible(false);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }

                else if (ime.equals("admin") && lozinka.equals("admin")){
                    try {
                        AdminOkvir();
                        container.setVisible(false);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }


            });

            //Definiranje okvira sučelja
            container = new JFrame();
            container.add(userNameLable);
            container.add(userNameTextField);
            container.add(passwordLabel);
            container.add(passwordField);
            container.add(loginButton);
            container.setLayout(null);
            container.setTitle("Pičenator 3000 by Marko Franković");
            container.setLocationByPlatform(true);
            container.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            container.setResizable(true);
            container.pack();
            container.setVisible(true);
            container.setMinimumSize(new Dimension(400, 200));
        }

    }
        //Instanciranje objekta i pozivanje sučelja
        public static void main(String[] args) throws IOException, LineUnavailableException, UnsupportedAudioFileException {
        Login Picenator = new Login();
        Picenator.LoginOkvir();

    }
}
